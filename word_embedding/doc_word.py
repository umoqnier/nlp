import numpy as np
import os
from nltk import SnowballStemmer
import re


def get_corpus():
    data = dict()
    paths = os.walk('CorpusDocs/')
    files = paths.__next__()[2]
    for file in files:
        with open('CorpusDocs/' + file, 'r') as f:
            data[file.strip()] = f.read()
    return data


def stemming(corpus):
    data = dict()
    clean_text = ''
    stemmer = SnowballStemmer("spanish")  # Setting language to the Stemming algorithm
    for file, text in zip(corpus.keys(), corpus.values()):
        clean_text += re.sub("[^A-Za-záéíóú\ ]+", r'', text).lower() + ' '  # Removing special symbols
        data[file] = [stemmer.stem(word) for word in clean_text.split()]  # Apply algorithm to each word
        clean_text = ""
    return data


def main():
    corpus = stemming(get_corpus())  # Get corpus and apply Stemming Algorithm
    

if __name__ == '__main__':
    main()
