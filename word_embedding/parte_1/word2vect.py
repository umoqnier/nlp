#! /usr/bin/python3
# -- coding: utf-8 -*-

import os  # to get corpus
import re  # to clean corpus
from nltk.stem import SnowballStemmer  # to apply stemming
import numpy as np
from collections import defaultdict


def clean_word(w):
        return re.sub("[^A-Za-záéíóú\ ]+", r'', w).lower()


def softmax(x, y, U):
    return np.exp(-np.dot(x, y))/np.sum(np.exp(-np.dot(U, y)))


def vocab():
    dicc = defaultdict()
    dicc.default_factory = lambda: len(dicc)
    return dicc


def BoW(corpus, vocab):
    for w in corpus:
        yield[vocab[w]]


# Loading corpus
corpus_folder_name = 'corpus/'
corpus = set()
documents = {}

# For stemming
stemmer = SnowballStemmer("spanish")
corpus = set(stemmer.stem(w) for w in corpus)

files = os.listdir(corpus_folder_name)
counter_documents = 0
for file in files:
    with open(corpus_folder_name + file, 'r') as f:
        documents[counter_documents] = []
        for l in f.readlines():
            for w in l.split(' '):
                word = stemmer.stem(clean_word(w))
                documents[counter_documents].append(word)
                corpus.add(word)
    counter_documents += 1
v = vocab()
words = list(BoW(corpus, v)) # Changed error

N = len(corpus)
hidden_neurons = 100
M = len(documents)

W = np.random.random((N, hidden_neurons))
U = np.random.random((M, hidden_neurons))


print('Entrenando red...')

eta = 1e-9
err = 10
it = 0
while err > 1e-6 and it < 10000:
    print('Error {0} -- It: {1}'.format(err, it))
    errores = []
    for doc in range(0, len(documents)):
        for w in v:
            # print('Doc: {0} -- Word: {1}'.format(doc, w))
            p = softmax(W[v[w]], U[doc], U)
            if w in documents[doc]:
                err = 1 - p
                errores.append(err)
            else:
                err = 0 - p
            # print('Prob {0} -- Err {1}'.format(p, err))
            U.T[doc] -= eta*err*W[v[w]]
        W[v[w]] -= eta*U.sum(1)
    it += 1
    err = max(errores)
print('Finish!')