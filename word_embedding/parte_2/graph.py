#! /usr/bin/python3
# -- coding: utf-8 -*-

"""
Universidad Nacional Autonoma de Mexico
Procesamiento del Lenguaje Natural

Practica 3
Algoritmo similar a Word2Vect donde se tienen una lengua mapeada a otra
representada mediante una grafica.
El corpus se encuentra en el archivo graph.txt con el siguiente formato:
    w1
        w2
        w3
    w4
        w5
        w6
        .
        .
Donde w1 es una palabra en el lenguaje origen y se encuentra conectada
con w2 y w3 del lenguaje destino

Equipo:
    Diego Alberto Barriga Martinez <umoqnier@gmail.com>
    Luis Alberto Oropeza Vilchis <luis.oropeza.129@gmail.com>

Dependencias:
    numpy
    sklearn
    matplotlib
    Si hay problemas con python3-tk instalar con apt-get install python3-tk en distribuciones basadas en ubuntu
"""

import re  # to clean corpus
import numpy as np  # To matrix processing
import pickle
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt


def clean_word(w):
        return re.sub("[^A-Za-záéíóú ]+", r'', w).lower()


def softmax(x, y):
    return np.exp(-np.dot(x, y))/np.sum(np.exp(-np.dot(U.T, y)))


def map_connections(connections, source, target):
    m = {}
    for c in connections:
        c = c.split()
        m[c[0]] = [target[w] for w in c[1:]]
    return m


def load_weight():
    try:
        f = open("./W.obj", 'rb')
        weight = pickle.load(f)
        return weight
    except FileNotFoundError:
        return 0


def plot_words(matrix, ids):  # La funcion recibe la matriz de datos y los ids de cada uno de los elementos
    """Taked from https://github.com/VMijangos/PLN/blob/master/Spectral.ipynb"""
    r = 0
    plt.scatter(matrix[:, 0], matrix[:, 1], marker='o', c='g')
    for label, x, y in zip(ids, matrix[:, 0], matrix[:, 1]):
        plt.annotate(label, xy=(x,y), xytext=(-1, 1), textcoords='offset points', ha='center', va='bottom')
        r += 1
    plt.show()


# Loading corpus
corpus_filename = 'graph.txt'
source_language = set()
target_language = set()
connections = []

with open(corpus_filename, 'r') as stream:
    conn = ''
    for line in stream.readlines():
        word = clean_word(line)
        if '\t' not in line:
            if conn != '':
                connections.append(conn)
            source_language.add(word)
            conn = word
        else:
            target_language.add(word)
            conn += ' {0}'.format(word)
    connections.append(conn)

# Creating indexes
source_language = {word: index for (word, index) in zip(source_language, range(0, len(source_language)))}
target_language = {word: index for (word, index) in zip(target_language, range(0, len(target_language)))}
words = map_connections(connections, source_language, target_language)

W = load_weight()  #  Load previous weights
if not W.any():

    # Network parameters
    M, N, d = len(source_language), len(target_language), 100

    # Weights
    W = np.random.random((M, d))
    U = np.random.random((d, N))

    print('Source Language size: {0}'.format(M))
    print('Target Language size: {0}'.format(N))
    print('Neurons from Hidden Layer: {0}'.format(d))

    # Training parameters
    eta = 1e-3
    epoch = 0
    limit = 5

    print('Learning rate: {0}'.format(eta))
    print('Maximum iterations: {0}'.format(5))
    print('Training Net...')

    # ToDo: put stop condition
    while epoch < limit:
        print('\t\tIteration {0}'.format(epoch))
        error_net = []
        for w1 in source_language:
            for w2 in target_language:
                p = softmax(U.T[target_language[w2]], W[source_language[w1]])
                if target_language[w2] in words[w1]:
                    error = 1 - p
                    print('{3}. p({0}|{1}) = {2}'.format(w2, w1, p, source_language[w1]))
                    # print('p({0}|{1}) = {2} -- {3}'.format(target_language[w2], source_language[w1], p, words[w1]))
                else:
                    error = 0 - p
                U.T[target_language[w2]] -= eta * error * W[source_language[w1]]
            W[source_language[w1]] -= eta*U.sum(1)
        epoch += 1

    obj_writer = open("./W.obj", 'wb')
    pickle.dump(W, obj_writer)
    obj_writer.close()

# Begin PCA
pca = PCA(n_components=2)
pca.fit(W)
W_reduce = pca.transform(W)
plot_words(W_reduce, target_language.keys())
